#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

PACKAGE = pianofish
PYTHON := python3

VERSION := $(shell rpmspec -q --qf '%{VERSION}' $(PACKAGE).spec | cut -d' ' -f1)
ifndef VERSION
version=$(shell sed -n '/^Version:\s\+/s/^Version:\s\+\(.[0-9]\)/\1/p' < $(PACKAGE).spec)
VERSION=$(shell /bin/echo $(version))
endif
ifndef VERSION
$(error VERSION must be set!)
endif

RELEASE := $(shell rpmspec -q --qf '%{RELEASE}' $(PACKAGE).spec | sed -e 's/\.centos$$//' -e 's/\.[^.]*$$//')
VER_REL := $(VERSION)-$(RELEASE)
PIPVERSION := $(shell $(PYTHON) setup.py --version)
ARCH := $(shell uname -m)

RPMDIRS = $(addprefix rpm/, SOURCES BUILD RPMS SRPMS)
RPM_TARBALL = rpm/SOURCES/$(PACKAGE)-$(VER_REL).tar.gz

DEB_BUILDDIR=build_debian
DEB_TARBALL=$(PACKAGE)_$(VERSION).orig.tar.gz
DEB_SRCDIR=$(DEB_BUILDDIR)/$(PACKAGE)-$(VERSION)

PYSRC = $(wildcard src/*.py src/*/*.py)

all: translations doc

local: all src/gschemas.compiled

src/gschemas.compiled: src/org.fedora.$(PACKAGE).gschema.xml
	cd src; glib-compile-schemas .

$(RPMDIRS):
	mkdir -p $(RPMDIRS) || :

$(RPM_TARBALL): $(RPMDIRS) $(PACKAGE).spec
	git archive --format=tar --prefix=$(PACKAGE)-$(VER_REL)/ HEAD | gzip -9 > $@

tarball: $(RPM_TARBALL)

rpm: $(RPM_TARBALL)
	rpmbuild -ba --define "_topdir $(PWD)/rpm" $(PACKAGE).spec

srpm: $(RPM_TARBALL)
	rpmbuild -bs --define "_topdir $(PWD)/rpm" $(PACKAGE).spec

$(DEB_BUILDDIR):
	mkdir -p $@

$(DEB_BUILDDIR)/$(DEB_TARBALL): $(DEB_BUILDDIR)
	git archive --format=tar --prefix=$(PACKAGE)-$(VERSION)/ HEAD | gzip -9 > $(DEB_BUILDDIR)/$(DEB_TARBALL)

$(DEB_SRCDIR): $(DEB_BUILDDIR)/$(DEB_TARBALL)
	cd $(DEB_BUILDDIR); tar -xf $(DEB_TARBALL)

deb-tarball: $(DEB_BUILDDIR)/$(DEB_TARBALL)

deb: $(DEB_SRCDIR)
	cd $(DEB_SRCDIR); dpkg-buildpackage -us -uc

clean:
	rm -rf rpm build dist src/gschemas.compiled translations/locale/en_US@piglatin src/pianofish.egg-info $(DEB_BUILDDIR)
	rm -f doc/*_doc_*.png doc/*.xhtml doc/*~
	find translations/locale -type f -name \*.mo -delete

translations/messages.pot: $(PYSRC)
	$(eval TMP := $(shell mktemp))
	$(PYTHON) setup.py extract_messages -c TRANSLATORS: -o $(TMP)
	sed -e "s/FIRST AUTHOR <EMAIL@ADDRESS>/Guy Streeter <guy.streeter@gmail.com>/" <$(TMP) >"$@"
	rm $(TMP)

translations/locale/en_US/LC_MESSAGES/$(PACKAGE).po: translations/messages.pot
	$(PYTHON) setup.py init_catalog

translations/locale/en_US/LC_MESSAGES/$(PYTHON)-$(PACKAGE).mo: translations/locale/en_US/LC_MESSAGES/$(PACKAGE).po
	$(PYTHON) setup.py compile_catalog

translations: translations/locale/en_US/LC_MESSAGES/$(PYTHON)-$(PACKAGE).mo

piglatin: translations
	mkdir -p translations/locale/en_US@piglatin/LC_MESSAGES
	$(PYTHON) mkpiglatin.py translations/locale/en_US/LC_MESSAGES/$(PACKAGE).po > translations/locale/en_US@piglatin/LC_MESSAGES/$(PACKAGE).po
	msgfmt translations/locale/en_US@piglatin/LC_MESSAGES/$(PACKAGE).po -o translations/locale/en_US@piglatin/LC_MESSAGES/$(PACKAGE).mo

git-tag:
	git tag -s $(VERSION)-$(RELEASE)

doc/Pianofish\ User\ Guide.pdf: doc/Pianofish\ User\ Guide.lyx
	lyx -e pdf2 "$<"

pdf: doc/Pianofish\ User\ Guide.pdf

dist/$(PACKAGE)-$(PIPVERSION).tar.gz:
	$(PYTHON) setup.py sdist

sdist: dist/$(PACKAGE)-$(PIPVERSION).tar.gz

dist/$(PACKAGE)-$(PIPVERSION).linux-$(ARCH).tar.gz:
	$(PYTHON) setup.py bdist

bdist: dist/$(PACKAGE)-$(PIPVERSION).linux-$(ARCH).tar.gz
