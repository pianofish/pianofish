#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
import os
import gettext

# for un-installed testing
try:
    __ld = os.path.dirname(__file__) + "/../translations/locale"
    if not os.path.isdir(__ld):
        __ld = None
except Exception:
    __ld = None

# This fake piglatin translation can be used to test that all
# displayed text is properly set up for translation. Change
# False to True, run "make piglatin", then run locally
# using run_local.sh and run_local_server.sh in separate terminals
if False:
    __translation = gettext.translation(
        "pianofish", __ld, languages=["en_US@piglatin"], fallback=True
    )
else:
    __translation = gettext.translation("pianofish", __ld, fallback=True)

__translation.install()

del __ld
del __translation
