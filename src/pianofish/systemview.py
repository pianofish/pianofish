#!/usr/bin/python3
#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#


from gi import require_version as gi_require_version

gi_require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gio, GLib, Gdk

from pianofish import translation

del translation
from pianofish.pciview import PCIView
from pianofish.utilities import Signaler, Topology, AppQuitException
from pianofish.bitmap import AffinityBitmap

import hwloc
import procfs
import threading


class UsageLevelBar(Gtk.LevelBar):
    def __init__(self, *args, **kwargs):
        super().__init__(
            max_value=100.0,
            min_value=0.0,
            value=0.0,
            can_focus=False,
            *args,
            **kwargs
        )
        self._usage = 0.0

    @GObject.Property(type=float, default=0.0, minimum=0.0, maximum=100.0)
    def usage(self) -> float:
        return self._usage

    @usage.setter
    def usage(self, value: float) -> None:
        self._usage = value
        self.set_value(value)


class _ObjectView(Gtk.Grid):
    pass  # just a forward reference for the IDE


def _obj_index_string(obj):
    objstring = obj.type_asprintf(0)
    if objstring.startswith("Group"):
        objstring = objstring[len("Group") :]
    if obj.depth != 0:
        if obj.os_index != hwloc.UINT_MAX:
            objstring = str(obj.os_index)
    return objstring


class _ListRow(Gtk.Grid):
    def __init__(
        self,
        width_chars: int,
        obj: hwloc.Obj,
        view: _ObjectView,
        *args,
        **kwargs
    ):
        super().__init__(
            name="_ListRow",
            column_homogeneous=False,
            vexpand=False,
            hexpand=True,
            valign="center",
            halign="fill",
            orientation="horizontal",
            column_spacing=5,
            border_width=5,
            *args,
            **kwargs
        )
        self.add(
            Gtk.Label(
                name="indexLabel",
                margin=0,
                visible=True,
                label=_obj_index_string(obj),
                width_chars=width_chars,
                xalign=1.0,
                lines=1,
                expand=False,
                halign="end",
            )
        )
        self._level_bar = UsageLevelBar(
            visible=True, vexpand=False, hexpand=True, halign="fill"
        )
        self.bind_property("usage", self._level_bar, "usage")
        self.add(self._level_bar)
        self._check_button = Gtk.CheckButton(
            name="rowCheck",
            visible=True,
            active=True,
            sensitive=False,
            halign="end",
        )
        self.add(self._check_button)
        self._check_button.connect("toggled", self._on_check_button_toggled)
        self._usage = 0.0
        self._selection_mode = False
        self._hwloc_obj = obj
        self._objectview = view
        view.bind_property(
            "usage", self, "usage", GObject.BindingFlags.SYNC_CREATE
        )

    def __repr__(self):
        string = self._hwloc_obj.type_string + " "
        string += str(self._hwloc_obj.os_index) + "\n\t" + super().__repr__()
        return string

    @property
    def view(self) -> _ObjectView:
        return self._objectview

    @GObject.Property(type=float, default=0.0, minimum=0.0, maximum=100.0)
    def usage(self) -> float:
        return self._usage

    @usage.setter
    def usage(self, value: float) -> None:
        self._usage = value

    @GObject.Property(type=bool, default=False)
    def selection_mode(self) -> bool:
        return self._selection_mode

    @selection_mode.setter
    def selection_mode(self, value):
        self._selection_mode = value
        self.view.selection_changed = False
        self._check_button.set_sensitive(value)

    def _on_check_button_toggled(self, _widget) -> None:
        self._check_button.set_inconsistent(False)
        checked = self._check_button.get_active()
        self.set_children_selected(checked)
        self.view.selection_changed = True

    def update_usage(self) -> float:
        return self._objectview.update_usage()

    def hide_siblings(self) -> None:
        self._objectview.hide_siblings(self)

    def show_stack(self, visible: bool) -> None:
        self._objectview.show_stack(visible)

    def set_children_selected(self, selected: bool) -> None:
        bitmask = AffinityBitmap(self._hwloc_obj.complete_cpuset)
        if selected:
            _TopologyView.SelectionBitmap |= bitmask
        else:
            _TopologyView.SelectionBitmap &= ~bitmask

    def get_children_selected(self) -> AffinityBitmap:
        bits = _TopologyView.SelectionBitmap & self._hwloc_obj.complete_cpuset
        self._check_button.set_inconsistent(False)
        if bits == self._hwloc_obj.complete_cpuset:
            self._check_button.set_active(True)
        elif bits.iszero:
            self._check_button.set_active(False)
        else:
            self._check_button.set_inconsistent(True)
        return bits


def _title_string(obj, verbose=0):
    objstring = obj.type_asprintf(verbose)
    if objstring.startswith("Group"):  # text comes from hwloc - not translated
        subgroup = obj.first_child.type_asprintf(0)
        return _("Group of {subgroup:s}").format(subgroup=subgroup)
    if obj.depth != 0:
        if obj.os_index != hwloc.UINT_MAX:
            objstring += " " + str(obj.os_index)
    attr = obj.attr_asprintf(" ", verbose)
    if attr:
        objstring += " (" + attr + ")"
    if obj.arity != 0:
        thisgroup = objstring
        # type names are untranslated, so adding 's' for plural works
        childtypes = obj.first_child.type_asprintf(0) + "s"
        return _("{childtypes:s} in {thisgroup:s}").format(
            thisgroup=thisgroup, childtypes=childtypes
        )
    return objstring


class _ObjectView(Gtk.Grid):
    def __init__(
        self,
        obj: hwloc.Obj,
        stack: Gtk.Stack,
        parent: _ObjectView,
        *args,
        **kwargs
    ):
        super().__init__(
            vexpand=False,
            hexpand=True,
            column_homogeneous=False,
            row_homogeneous=False,
            column_spacing=3,
            border_width=3,
            valign="start",
            orientation="vertical",
            *args,
            **kwargs
        )
        self._parent_view = parent
        self._hwloc_obj = obj
        self._usage = 0.0
        if obj.arity != 0:
            self._label = Gtk.Label(
                name="smallLabel",
                label=_title_string(obj),
                visible=True,
                expand=False,
                halign="center",
                valign="center",
                can_focus=False,
                margin=5,
            )
            self.add(self._label)
            width_chars = len(str(max([c.os_index for c in obj.children])))
        else:
            width_chars = -1
        self._listbox = Gtk.ListBox(visible=True, selection_mode="none")
        self.foreach = self._listbox.foreach
        stack.add_titled(self, self.name, self.title)
        for child in obj.children:
            if child.depth < 0 or child.type not in _VIEWS_BY_TYPE:
                continue
            view = _VIEWS_BY_TYPE[child.type](child, stack, self, visible=False)
            self.bind_property("selection-mode", view, "selection-mode")
            view.bind_property("selection-changed", self, "selection-changed")
            listrow = _ListRow(width_chars, child, view, visible=True)
            listboxrow = Gtk.ListBoxRow(
                visible=True, activatable=child.arity != 0
            )
            listboxrow.add(listrow)
            self._listbox.insert(listboxrow, -1)
            self.bind_property("selection-mode", listrow, "selection-mode")
        self.add(self._listbox)
        self._listbox.connect("row-activated", self._on_row_activated)
        self._usage = 0.0
        self._selection_mode = False

    def __repr__(self):
        string = self._hwloc_obj.type_string + " "
        string += str(self._hwloc_obj.os_index) + "\n\t" + super().__repr__()
        return string

    def _on_row_activated(self, widget: Gtk.ListBox, row: Gtk.ListBoxRow):
        _TopologyView.RowActivated.emit_signal(row.get_child())

    @property
    def name(self) -> str:
        # We don't use the name, but need it to be unique
        return super().__repr__()

    @property
    def title(self) -> str:
        return self._hwloc_obj.type_string + " " + str(self._hwloc_obj.os_index)

    @GObject.Property(type=float, default=0.0, minimum=0.0, maximum=100.0)
    def usage(self) -> float:
        return self._usage

    @usage.setter
    def usage(self, value: float) -> None:
        self._usage = value

    @GObject.Property(type=bool, default=False)
    def selection_mode(self) -> bool:
        return self._selection_mode

    @selection_mode.setter
    def selection_mode(self, value):
        self._selection_mode = value
        self.selection_changed = False

    @GObject.Property(type=bool, default=False)
    def selection_changed(self) -> bool:
        return self._selection_changed

    @selection_changed.setter
    def selection_changed(self, value: bool) -> None:
        self._selection_changed = value
        self._get_selected_bits()

    def _get_selected_bits(self) -> AffinityBitmap:
        bits = AffinityBitmap()

        def get_row_bits(row: Gtk.ListBoxRow, bits: AffinityBitmap) -> None:
            bits &= row.get_child().get_children_selected()

        self.foreach(get_row_bits, bits)
        return bits

    def set_child_bits(self, selected: bool) -> None:
        bits = self._hwloc_obj.complete_cpuset
        if selected:
            _TopologyView.SelectionBitmap |= bits
        else:
            _TopologyView.SelectionBitmap &= ~bits
        self.selection_changed = True

    def update_usage(self) -> float:
        usage = []

        def usages(row: Gtk.ListBoxRow, ulist: []) -> None:
            ulist.append(row.get_child().update_usage())

        self._listbox.foreach(usages, usage)
        try:
            self.usage = sum(usage) / len(usage)
        except Exception:
            self.usage = 0.0
        return self.usage

    def hide_siblings(self, not_this: _ListRow) -> None:
        parent = self._parent_view
        if parent is None:
            return

        def hide_this(boxrow: Gtk.ListBoxRow, but_not: _ListRow):
            row: _ListRow = boxrow.get_child()
            if row is but_not:
                return
            row.show_stack(False)

        parent.foreach(hide_this, not_this)

    def show_stack(self, visible: bool) -> None:
        self.set_visible(visible)
        if visible:
            return

        def show_this(boxrow: Gtk.ListBoxRow, _data) -> None:
            row: _ListRow = boxrow.get_child()
            row.show_stack(False)

        self.foreach(show_this, None)


class _PUView(_ObjectView):
    def __init__(self, obj: hwloc.Obj, *args, **kwargs):
        super().__init__(obj, *args, **kwargs)

    def update_usage(self):
        try:
            self.usage = float(
                _TopologyView.CPUStats[self._hwloc_obj.os_index + 1].usage
            )
        except Exception:
            self.usage = 0.0
        return self.usage


_MachineView = _ObjectView
_NodeView = _ObjectView
_SocketView = _ObjectView
# _CacheView = _ObjectView
_CoreView = _ObjectView
_GroupView = _ObjectView
_MiscView = _ObjectView

_VIEWS_BY_TYPE = {
    hwloc.OBJ_MACHINE: _MachineView,
    hwloc.OBJ_NUMANODE: _NodeView,
    hwloc.OBJ_PACKAGE: _SocketView,
    # hwloc.OBJ_CACHE: _CacheView,
    hwloc.OBJ_CORE: _CoreView,
    hwloc.OBJ_PU: _PUView,
    hwloc.OBJ_GROUP: _GroupView,
    hwloc.OBJ_MISC: _MiscView,
}


class CPUSelectionLabel(Gtk.Label):
    def __init__(self, *args, **kwargs):
        super().__init__(
            name="smallLabel",
            label="",
            single_line_mode=True,
            track_visited_links=False,
            justify=Gtk.Justification.CENTER,
            focus_on_click=False,
            *args,
            **kwargs
        )
        self._selection_string = ""

    @GObject.Property(type=str, default="")
    def selection_string(self):
        return self._selection_string

    @selection_string.setter
    def selection_string(self, value):
        self._selection_string = value
        self.set_label(_("CPU Selection: {cpulist:s}").format(cpulist=value))


class _AppHeaderBar(Gtk.HeaderBar):
    def __init__(self, *args, **kwargs):
        super().__init__(
            title=_("System View"),
            show_close_button=False,
            vexpand=False,
            halign="fill",
            *args,
            **kwargs
        )
        if Gtk.check_version(3, 12, 0) is None:
            self.set_has_subtitle(False)
        self.hide_button = Gtk.Button(
            visible=True, expand=False, valign="center"
        )
        self.hide_button.set_image(
            Gtk.Image(
                icon_name="window-close-symbolic", icon_size=Gtk.IconSize.MENU
            )
        )
        self.pack_end(self.hide_button)


class _EditButtonBox(Gtk.ButtonBox):
    def __init__(self, *args, **kwargs):
        super().__init__(layout_style="start", *args, **kwargs)
        self.select_all_button = Gtk.Button(
            visible=True, halign="start", label=_("Select all")
        )
        self.add(self.select_all_button)
        self.set_child_non_homogeneous(self.select_all_button, True)
        self.unselect_all_button = Gtk.Button(
            visible=True, halign="start", label=_("Select none")
        )
        self.add(self.unselect_all_button)
        self.set_child_non_homogeneous(self.unselect_all_button, True)


class _TopologyView(Gtk.Grid):
    RowActivated = Signaler("RowActivated", (_ListRow,))
    CPUStats = procfs.cpusstats()
    SelectionBitmap = AffinityBitmap(Topology.root_obj.complete_cpuset)

    def __init__(self, *args, **kwargs):
        super().__init__(
            name="_TopologyView",
            column_homogeneous=True,
            orientation="vertical",
            expand=True,
            halign="fill",
            *args,
            **kwargs
        )
        box = Gtk.Box(visible=True, orientation="horizontal", homogeneous=False)
        self._switcher = Gtk.StackSwitcher(
            name="_TopoSwitcher",
            visible=True,
            homogeneous=True,
            hexpand=True,
            halign="center",
            valign="center",
            margin=10,
        )
        box.set_center_widget(self._switcher)
        self._edit_button = Gtk.Button(
            visible=True,
            no_show_all=True,
            focus_on_click=False,
            margin_start=5,
            margin_end=0,
            valign="center",
            halign="end",
            relief="none",
        )
        img = Gtk.Image(
            icon_name="object-select-symbolic",
            icon_size=Gtk.IconSize.LARGE_TOOLBAR,
        )
        self._edit_button.set_image(img)
        self.bind_property(
            "selection-mode",
            self._edit_button,
            "visible",
            (
                GObject.BindingFlags.INVERT_BOOLEAN
                | GObject.BindingFlags.SYNC_CREATE
            ),
        )
        self._edit_button.connect("clicked", self._on_edit_button_clicked)
        if Gtk.Widget.get_default_direction() != Gtk.TextDirection.RTL:
            uname = "edit-undo-symbolic"
        else:
            uname = "edit-undo-rtl-symbolic"
        box.pack_end(self._edit_button, False, False, 0)
        self._cancel_button = Gtk.Button(
            visible=False,
            no_show_all=True,
            focus_on_click=False,
            margin_start=1,
            margin_end=0,
            valign="center",
            halign="end",
        )
        self._cancel_button.set_image(
            Gtk.Image(icon_name=uname, icon_size=Gtk.IconSize.LARGE_TOOLBAR)
        )
        self._cancel_button.connect("clicked", self._on_cancel_button_clicked)
        self.bind_property(
            "selection-mode",
            self._cancel_button,
            "visible",
            GObject.BindingFlags.SYNC_CREATE,
        )
        box.pack_end(self._cancel_button, False, False, 0)
        self._stack = Gtk.Stack(
            visible=True,
            halign="fill",
            hexpand=True,
            valign="start",
            vexpand=False,
        )
        self._accept_button = Gtk.Button(
            visible=False,
            no_show_all=True,
            focus_on_click=False,
            margin_start=0,
            margin_end=0,
            halign="end",
            valign="center",
            label=_("OK"),
        )
        box.pack_end(self._accept_button, False, False, 0)
        self.add(box)
        self._switcher.set_stack(self._stack)
        scrolled_window = Gtk.ScrolledWindow(visible=True, expand=True)
        scrolled_window.add(self._stack)
        self.add(scrolled_window)
        self._usage = 0.0
        self._top_view = _VIEWS_BY_TYPE[Topology.root_obj.type](
            Topology.root_obj, self._stack, None, visible=True
        )
        self._top_view.bind_property(
            "usage", self, "usage", GObject.BindingFlags.SYNC_CREATE
        )
        self.bind_property("selection-mode", self._top_view, "selection-mode")
        self._top_view.bind_property(
            "selection-changed", self, "selection-changed"
        )
        self._selected_bitmap = AffinityBitmap(
            Topology.root_obj.complete_cpuset
        )
        self._selection_string = self._selected_bitmap.get_friendly_text()
        self._edit_button_box = _EditButtonBox(visible=True)
        self._accept_button.connect("clicked", self._on_accept_button_clicked)
        self.bind_property(
            "selection-changed", self._accept_button, "sensitive"
        )
        self.bind_property("selection-mode", self._accept_button, "visible")
        self._edit_button_box.select_all_button.connect(
            "clicked", self._on_select_all_button_clicked
        )
        self._edit_button_box.unselect_all_button.connect(
            "clicked", self._on_unselect_all_button_clicked
        )
        revealer = Gtk.Revealer(
            visible=True,
            transition_type="slide-up",
            expand=False,
            valign="end",
            halign="fill",
        )
        revealer.add(self._edit_button_box)
        self.bind_property("selection-mode", revealer, "reveal-child")
        self.add(revealer)
        self._statusbar = CPUSelectionLabel(
            visible=True, selection_string=self._selection_string
        )
        self.add(self._statusbar)

        self.RowActivated.connect_handler(self._on_row_activated)
        self._in_edit_mode = False
        self._selection_changed = False

        def preload_usage() -> None:
            self.CPUStats.reload()  # I/O happens here

        threading.Thread(target=preload_usage).start()

    @GObject.Property(type=float, default=0.0, minimum=0.0, maximum=100.0)
    def usage(self) -> float:
        return self._usage

    @usage.setter
    def usage(self, value: float) -> None:
        self._usage = value

    @GObject.Property(type=bool, default=False)
    def selection_mode(self) -> bool:
        return self._selection_mode

    @selection_mode.setter
    def selection_mode(self, value):
        self._selection_mode = value
        self.selection_changed = False

    @GObject.Property(type=bool, default=False)
    def selection_changed(self) -> bool:
        return self._selection_changed

    @selection_changed.setter
    def selection_changed(self, value: bool) -> None:
        if self._selected_bitmap == self.SelectionBitmap:
            value = False
        selection = self.SelectionBitmap.get_friendly_text()
        self._statusbar.selection_string = selection
        self._selection_changed = value
        self.selection_string = selection
        ctx = self._cancel_button.get_style_context()
        if value:
            ctx.add_class("destructive-action")
        else:
            ctx.remove_class("destructive-action")
        ctx = self._accept_button.get_style_context()
        if value:
            ctx.add_class("suggested-action")
        else:
            ctx.remove_class("suggested-action")

    @GObject.Property(type=str, default="")
    def selection_string(self) -> str:
        return self._selection_string

    @selection_string.setter
    def selection_string(self, value: str) -> None:
        self._selection_string = value

    def _on_edit_button_clicked(self, _widget) -> None:
        self.SelectionBitmap.copy(self._selected_bitmap)
        self.selection_mode = True

    def _on_cancel_button_clicked(self, _widget) -> None:
        self.SelectionBitmap.copy(self._selected_bitmap)
        self.selection_mode = False

    def _on_accept_button_clicked(self, _widget):
        self.selection_mode = False
        self._selected_bitmap.copy(self.SelectionBitmap)

    def _on_select_all_button_clicked(self, _widget) -> None:
        view: _ObjectView = self._stack.get_visible_child()
        view.set_child_bits(True)

    def _on_unselect_all_button_clicked(self, _widget) -> None:
        view: _ObjectView = self._stack.get_visible_child()
        view.set_child_bits(False)

    def _on_row_activated(self, _signaler: Signaler, listrow: _ListRow) -> None:
        listrow.hide_siblings()
        listrow.show_stack(True)
        self._stack.set_visible_child(listrow.view)

    def refresh(self) -> bool:
        def update_usage() -> bool:
            self._top_view.update_usage()
            return GLib.SOURCE_REMOVE

        def reload_cpusstats() -> None:
            self.CPUStats.reload()  # I/O happens here
            # don't change the GUI outside the GTK main loop
            GLib.idle_add(update_usage)

        # start a Python thread and let the main loop continue
        threading.Thread(target=reload_cpusstats).start()


class SelectionWindow(Gtk.ApplicationWindow):
    def __init__(self):
        super().__init__(
            title=_("CPU Selection"),
            show_menubar=False,
            default_height=500,
            default_width=400,
        )
        if Topology.complete_cpuset is None:
            text = _(
                "Pianofish can only operate on the topology of a single system"
            )
            text2 = _("The application will now exit")
            errdialog = Gtk.MessageDialog(
                parent=self.get_toplevel(),
                title="pianofish",
                text=text,
                secondary_text=text2,
                buttons=Gtk.ButtonsType.CLOSE,
            )
            errdialog.run()
            errdialog.destroy()
            raise AppQuitException
        headerbar = _AppHeaderBar(visible=True)
        headerbar.hide_button.connect("clicked", self._on_hide_button_clicked)
        self.set_titlebar(headerbar)
        self._pciview = PCIView(margin=1, hexpand=True)
        scrolled_window = Gtk.ScrolledWindow(
            visible=True,
            vscrollbar_policy="automatic",
            hscrollbar_policy="never",
        )
        scrolled_window.add(self._pciview)
        self._topology_view = _TopologyView(visible=True)
        grid = Gtk.Grid(
            visible=True,
            orientation="horizontal",
            column_spacing=4,
            row_homogeneous=True,
            column_homogeneous=False,
        )
        grid.add(self._topology_view)
        grid.add(scrolled_window)
        self.add(grid)

        self._selection_string = ""
        self._usage = 0.0
        self._topology_view.bind_property(
            "selection-string",
            self,
            "selection-string",
            GObject.BindingFlags.SYNC_CREATE,
        )
        self._topology_view.bind_property("usage", self, "usage")

    @GObject.Property(type=float, default=0.0, minimum=0.0, maximum=100.0)
    def usage(self) -> float:
        return self._usage

    @usage.setter
    def usage(self, value: float) -> None:
        self._usage = value

    @GObject.Property(type=str)
    def selection_string(self) -> str:
        return self._selection_string

    @selection_string.setter
    def selection_string(self, value: str) -> None:
        self._selection_string = value
        self._pciview.update_selection(_TopologyView.SelectionBitmap)

    def _on_hide_button_clicked(self, _widget):
        self.hide()

    def refresh(self) -> None:
        self._topology_view.refresh()


class _Win(Gtk.ApplicationWindow):
    def __init__(self, application=None):
        super().__init__(visible=True, application=application)
        self._selection_label = Gtk.Button(label="click this", visible=True)
        self._selection_window = SelectionWindow()
        self._selection_window.bind_property(
            "selection_string", self._selection_label, "label"
        )
        self._selection_label.connect("clicked", self._on_button_clicked)
        self.add(self._selection_label)
        # self._selection_window.present()
        GLib.timeout_add_seconds(interval=3, function=self.refresh)

    def refresh(self) -> bool:
        self._selection_window.refresh()
        return GLib.SOURCE_CONTINUE

    def _on_button_clicked(self, _widget) -> None:
        self._selection_window.present_with_time(Gtk.get_current_event_time())
        self._selection_label.set_label(self._selection_window.selection_string)


class _App(Gtk.Application):
    def __init__(self):
        flags = (
            Gio.ApplicationFlags.NON_UNIQUE
            | Gio.ApplicationFlags.HANDLES_COMMAND_LINE
        )
        super(_App, self).__init__(
            application_id="org.fedora.testStack", flags=flags
        )
        GLib.set_application_name("testStack")
        GLib.set_prgname("testStack")
        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])
        csstext = b"""
        #_TopologyView #smallLabel {
            font-size: smaller;
        }
        #_TopoSwitcher label {
            font-size: smaller;
            font-weight: bold;
        }
        #_ListRow #indexLabel {
            font-size: larger;
        }
        #_ListRow #rowCheck check {
            min-width: 18px;
            min-height: 18px;
        }
        """
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(csstext)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

    def do_command_line(self, commandline):
        Gtk.Application.do_command_line(self, commandline)
        self.do_activate()
        return 0

    def _quit_activated(self, *_args):
        self.quit()

    def do_startup(self):
        Gtk.Application.do_startup(self)
        act = Gio.SimpleAction(name="quit")
        act.connect("activate", self._quit_activated)
        self.add_action(act)

    def do_activate(self):
        win = _Win()
        self.add_window(win)
        win.present()


if __name__ == "__main__":
    import sys

    sys.exit(_App().run(sys.argv))
