#
# Copyright 2019-202 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

%if 0%{?rhel} && 0%{?rhel} <= 6
%{error: RHEL < 7 is not supported}
%endif

%global source_date_epoch_from_changelog 1

%if 0%{?rhel} == 7
# The epel7 buildroot on COPR does not have Python3 installed, so
# the various python3 rpm macros are not available
%global python3_pkgversion 36
%global python3 python%{python3_pkgversion}
%global __python3 /usr/bin/%{python3}
%global py3_build %{__python3} setup.py build
%global py3_install %{__python3} setup.py install --skip-build --root %{buildroot}
%global python3_sitelib /usr/lib/python3.6/site-packages
%global babel2 python-babel
# No python3-babel in EPEL 7
%global babel3 %{nil}
# Versioned lincense dir name
%global licenseDirVer -%{version}
%else
%global babel2 python2-babel
%global python3 python3
%global babel3 python3-babel
%global licenseDirVer %{nil}
%endif

%global debug_package %{nil}
%{!?_licensedir:%global license %doc}

%global _release_string 2

Name:           pianofish
Version:        2.0
Release:        %{_release_string}%{?dist}
Summary:        experimental process management GUI

Group:          Applications/System
License:        GPLv2+
URL:            https://gitlab.com/pianofish/pianofish.git
Source0:        %{name}-%{version}-%{_release_string}.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  desktop-file-utils
BuildRequires:  %{python3}, %{python3}-devel, %{python3}-setuptools, %{babel3}
Requires:       %{python3}, python3-hwloc, %{python3}-gobject
Requires:       polkit
Requires:       python3-schedutils
Requires:       python3-hwloc >= 3.0
Requires:       python3-linux-procfs > 0.6.1-3

%description
experimental tuna GUI

%prep
%setup -q -n pianofish-%{version}-%{_release_string}

%build
%py3_build

%install
%if 0%{?_licensedir:1}
 LICENSEDIR=%_licensedir/pianofish%{licenseDirVer}
 export LICENSEDIR
%endif
%py3_install
desktop-file-install --dir=${RPM_BUILD_ROOT}%{_datadir}/applications src/pianofish.desktop
%if 0%{?rhel} && 0%{?rhel} == 7
%{warn: No translations built}
%else
%find_lang %{name}
%endif

%if 0%{?rhel} && 0%{?rhel} == 7
%files
%else
%files -f %{name}.lang
%endif
%defattr(-,root,root,-)
%{_bindir}/pianofish
%{_sysconfdir}/dbus-1/system.d/*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/pianofish
%{_docdir}/pianofish
%{_datadir}/polkit-1/actions/*
%{_datadir}/dbus-1/system-services/*
%{_datadir}/applications/*
%{_libexecdir}/*
%license COPYING
%license LICENSE
%{python3_sitelib}/*

%postun
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%changelog
* Fri Dec 20 2019 Guy Streeter <guy.streeter@gmail.com> 2.0-1
- Convert to hwloc version 2 for Fedora 31

* Tue Aug 13 2019 Guy Streeter <guy.streeter@gmail.com> 2.0-1
- build for epel7

* Mon Jul 29 2019 Guy Streeter <guy.streeter@gmail.com> 2.0-0
- updates for flake8 compiance
- changes for newer Python gobject introspection
- complete re-write of the cpu selection GUI

* Fri Mar 22 2019 Guy Streeter <guy.streeter@gmail.com> 1.5-0
- changed versioning
- fixed various Python3 issues
- fixed IRQ scheduler change bug
- added Debian build support

* Wed Sep 26 2018 Guy Streeter <guy.streeter@gmail.com> 1-4
- switch to Python3

* Mon Aug 13 2018 Guy Streeter <guy.streeter@gmail.com> 1-3
- flake8 formatting
- add option to limit process list to CPU selection

* Fri May 11 2018 Guy Streeter <guy.streeter@gmail.com> 1-2
- Check if cpuset is None instead of zero in PCI view

* Thu Feb  1 2018 Guy Streeter <guy.streeter@gmail.com> 1-1
- added last CPU location column
- added column header tooltips

* Mon Jan 15 2018 Guy Streeter <guy.streeter@gmail.com> 1-0.3
- completed changes for pip install

* Fri Dec 22 2017 Guy Streeter <guy.streeter@gmail.com> 1-0.2
- move setup.py to the top level for pip

* Wed Dec 20 2017 Guy Streeter <guy.streeter@gmail.com> 1-0.1
- corrected mnemonics in atrributes dialog

* Sun Dec 17 2017 Guy Streeter <guy.streeter@gmail.com> 1-0
- time to call this 1.0

* Thu Dec 14 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2.6
- fix initialization of set attribute dialog
- pytlint and PEP8 changes
- preserve the user guide pdf when cleaning up

* Mon Dec 11 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2.5
- correct policy string usage in IRQ attributes
- always set the priority if the policy needs one

* Mon Dec 11 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2.4
- realtime priorities are 1-99
- deliver user guide
- typo bug in IRQ attribute setting

* Mon Dec  4 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2.3
- correct the wording in the Distribute Tasks dialog

* Tue Nov 28 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2.2
- fix typos and bug in IRQ attribute setting

* Fri Nov 24 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2.1
- Requires python-schedutils

* Thu Nov 23 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.2
- new python-hwloc minimum version requirement
- refuse to run with multi-maachine topology
- correctly handle levels with no physical index
- do not skip useless levels

* Sun Nov 12 2017 Guy Streeter <guy.streeter@gmail.com> 0-6.1
- corrected tarfile name and path

* Sun Nov 12 2017 Guy Streeter <guy.streeter@gmail.com> 0-6
- added an option to use bash-style filter globs
- changed the filter button from Search to Find, and added a tooltip
- more fixes for displaying CPU access to PCI devices
- made Cancel and done translatable
- enabled mnemonics for Cancel and Done in AttributesDialog
- Used a default size for the SystemView window
- Enhanced the distribute function
- added an About dialog

* Thu Sep 21 2017 Guy Streeter <guy.streeter@gmail.com> 0-5
- Added distribute function

* Sun Jul  9 2017 Guy Streeter <guy.streeter@gmail.com> 0-4
- Removed the last dependencies on the tuna package

* Thu Feb  2 2017 Guy Streeter <guy.streeter@gmail.com> 0-3.1
- require python-linux-procfs >= 0.4.9

* Thu Jan 21 2016 Guy Streeter <streeter@redhat.com> - 0-2
- code cleanup and comments
- 0-1 version string is worn out

* Mon Jan 18 2016 Guy Streeter <streeter@redhat.com> - 0-1.10.1
- fix stale treeview instance pointer

* Fri Jan  8 2016 Guy Streeter <streeter@redhat.com> - 0-1.10
- add a d-bus backend

* Wed Jan  6 2016 Guy Streeter <streeter@redhat.com> - 0-1.9.4
- pep8 style coding changes

* Wed Jan  6 2016 Guy Streeter <streeter@redhat.com> - 0-1.9.3
- add a search-changed handler to update the process filter faster

* Mon Jan  4 2016 Guy Streeter <streeter@redhat.com> - 0-1.9.2
- License directory wierdness on RHEL7

* Fri Dec 18 2015 Guy Streeter <streeter@redhat.com> - 0-1.9.1
- Lots of dialog-box cleanup, mostly cosmetic

* Tue Dec 15 2015 Guy Streeter <streeter@redhat.com> - 0-1.9
- fix set_affinity error handling
- fix process attributes launch on unselected entry
- update the affinity checkbox as the affinit entry is changed
- add minimum size to the process list in the process atrributes dialog
- limit the IRQ list to single-selection
- clean up the attributes dialogs

* Thu Dec 10 2015 Guy Streeter <streeter@redhat.com> - 0-1.8.4
- correction to licensedir macro
- added dependencies

* Wed Nov 18 2015 Guy Streeter <streeter@redhat.com> - 0-1.8.3
- typo in .desktop file

* Mon Nov 16 2015 Guy Streeter <streeter@redhat.com> - 0-1.8.2
- minor display teaks
- simplify the attirbute choosers
- default "show cgroups" to False
- fix multiple selections bug for process list

* Fri Nov 13 2015 Guy Streeter <streeter@redhat.com> - 0-1.8.1
- stupid typo

* Fri Nov 13 2015 Guy Streeter <streeter@redhat.com> - 0-1.8
- Support for launching without root privilege

* Thu Nov 12 2015 Guy Streeter <streeter@redhat.com> - 0-1.7.3
- Fedora 23 pkexec refuses to work when exec'd

* Thu Nov 12 2015 Guy Streeter <streeter@redhat.com> - 0-1.7.2
- "helpful" IDE inserted a random import

* Thu Nov 12 2015 Guy Streeter <streeter@redhat.com> - 0-1.7.1
- add a .desktop file

* Wed Nov 11 2015 Guy Streeter <streeter@redhat.com> - 0-1.7
- put a preferences button on the headerbar
- optionally display cgroups

* Tue Nov 10 2015 Guy Streeter <streeter@redhat.com> - 0-1.6
- allow filter on just kernel or user threads

* Thu Nov  5 2015 Guy Streeter <streeter@redhat.com> - 0-1.5
- support dragging process selection to CPU selection to set affinity

* Wed Nov  4 2015 Guy Streeter <streeter@redhat.com> - 0-1.4.1
- fixed bailout on irq with pid

* Tue Nov  3 2015 Guy Streeter <streeter@redhat.com> - 0-1.4
- added process view filter

* Wed Oct 28 2015 Guy Streeter <streeter@redhat.com> - 0-1.3.1
- fixed display of threads

* Tue Oct 13 2015 Guy Streeter <streeter@redhat.com> - 0-1.3
- first basic working version.

* Tue Jun  9 2015 Guy Streeter <streeter@redhat.com> - 0-1
- Initial build
