#!/usr/bin/python3
#
# Copyright 2019 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

import fileinput

id_input = False


def get_quoted_text(string):
    return string[string.find('"') + 1: string.rfind('"')]


msgid = []
for line in fileinput.input():
    line = line.strip("\n")
    if id_input:
        if line.startswith('"'):
            msgid.append(get_quoted_text(line))
            print(line)
            continue
        id_input = False
    if line.startswith("msgid "):
        string = get_quoted_text(line)
        if len(string):
            msgid = [string]
        else:
            msgid = []
            id_input = True
        print(line)
        continue
    if line.startswith("msgstr "):
        if len(msgid) == 1:
            print('msgstr "' + msgid[0] + 'yay"')
        else:
            print(line)
            for msg in msgid:
                print('"' + msg + 'yay"')
        continue
    print(line)
